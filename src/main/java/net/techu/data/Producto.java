package net.techu.data;

public class Producto {

    public String id;
    public String nombre;
    public double precio;
    public String categoria;

    public Producto(String id, String nombre,
                    double precio,
                    String categoria) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.categoria = categoria;
    }

}
